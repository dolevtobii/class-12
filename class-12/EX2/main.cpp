#include "MessagesSender.h"

#define SIGNIN 1
#define SIGNOUT 2
#define CONNECT 3
#define EXIT 4

int main()
{
	MessagesSender* msg = new MessagesSender();
	int userChoice = 0;
	string userName = "";

	do
	{
		cout << "1- Signin \n2- Signout \n3- Connected Users \n4- Exit" << endl;
		cout << "Enter which action to do: ";
		cin >> userChoice;
		
		switch (userChoice)
		{
		case SIGNIN:
			cout << "Enter user name: ";
			cin >> userName;

			msg->signin(userName);

			break;

		case SIGNOUT:
			cout << "Enter user name: ";
			cin >> userName;

			msg->signout(userName);

			break;

		case CONNECT:
			msg->Connected_Users();
			break;

		case EXIT:
			cout << "GoodBye!" << endl;
			break;
		
		default:
			cout << "Wrong number! try again!" << endl;
		}
	
	} while (userChoice != EXIT);
	std::thread messageRead(&MessagesSender::read_data, msg);
	std::thread messageWrite(&MessagesSender::write_data, msg);

	messageRead.join();
	messageWrite.join();
	
	return 0;
}