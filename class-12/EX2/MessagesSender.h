#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <mutex>
#include <condition_variable>

using std::cout;
using std::cin;
using std::endl;
using std::string;

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void signin(string userName);
	void signout(string userName);
	void Connected_Users();

	void read_data();
	void write_data();

private:
	std::vector<string> messages;
	std::vector<string> users;
};

