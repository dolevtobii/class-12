#include "MessagesSender.h"

std::mutex m;

/*
	function is C'tor
	input: none
	output: none
*/
MessagesSender::MessagesSender()
{
}


/*
	function is D'tor
	input: none
	output: none
*/
MessagesSender::~MessagesSender()
{
}


/*
	function will add a user to the user list
	input: user name
	output: none
*/
void MessagesSender::signin(string userName)
{
	if(std::find(this->users.begin(), this->users.end(), userName) != this->users.end())
	{
		cout << "User already signed in!\n" << endl;
	}
	else
	{
		this->users.push_back(userName);
		cout << "User signin successfully!\n" << endl;
	}
}


/*
	function will remove a user from the user list
	input: user name
	output: none
*/
void MessagesSender::signout(string userName)
{
	std::vector<string>::iterator itr = std::find(this->users.begin(), this->users.end(), userName);

	if (itr != this->users.end())
	{
		this->users.erase(itr);
		cout << "User signout successfully!\n" << endl;
	}
	else
	{
		cout << "User not logined!\n" << endl;
	}
}


/*
	function will print the user list
	input: none
	output: none
*/
void MessagesSender::Connected_Users()
{
	int count = 1;
	std::vector<string>::iterator i;
	for (i = this->users.begin(); i != this->users.end(); i++, count++)
	{
		std::cout << "user " << count << ":  "<< *i << endl;
	}
	cout << "\n" << endl;
}

void MessagesSender::read_data()
{
	std::ifstream fileToRead("data.txt");
	string line = "";

	if (fileToRead.is_open())
	{
		m.lock();
		while(getline(fileToRead, line))
		{
			this->messages.push_back(line);
		}
		fileToRead.close();

		std::ofstream fileToWrite("data.txt");
		fileToWrite << " ";
		fileToWrite.close();
		m.unlock();
	}
}

void MessagesSender::write_data()
{
	std::ofstream fileToWrite("output.txt");
	int i = 0;
	int j = 0;
	
	m.lock();
	for (i = 0; i < this->messages.size(); i++)
	{
		for (j = 0; j < this->users.size(); j++)
		{
			fileToWrite << this->users[j] << ": " << this->messages[i] << endl;
		}
	}
	m.unlock();
	fileToWrite.close();
}
