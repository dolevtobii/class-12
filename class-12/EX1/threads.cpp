#include "threads.h"

std::mutex m;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	int j = 0;
	if (file.is_open())
	{
		m.lock();
		for (i = begin; i <= end; i++)
		{
			if (isPrime(i) == true)
			{
				file << i;
				file << "\n";
			}
		}
		file.close();
		m.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	std::thread* arr = new std::thread[N];
	int temp = (end - begin) / N;
	int i = 0;
	if (N == 1)
	{
		arr[0] = std::thread(writePrimesToFile, begin, end, ref(file));
	}

	for (i = 1; i < N; i++, begin += temp)
	{
		arr[i - 1] = std::thread(writePrimesToFile, begin, end, ref(file));
	}
	arr->join();

}


bool isPrime(int n)
{
	bool isPrime = true;
	if (n > 1)
	{
		for (int i = 2; i <= n / 2 && isPrime != false; i++)
		{
			if (n % i == 0) 
			{
				isPrime = false;
			}
		}
	}
	else
	{
		isPrime = false;
	}
	return isPrime;
}
