#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <time.h> 
#include<vector>
#include <mutex>


void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

bool isPrime(int n);